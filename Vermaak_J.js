//ID:10015918
//Date: 14/06/2018

//Definiton of "Van" class
class Van {
    //"default" getter and setter for the "_vanID" property
    get VanID(){
        return this._vanID;
    }
    set VanID(value){
        this._vanID = value;
    }

    //"default" getter and setter for the "_capacity" property
    get Capacity(){
        return this._capacity;
    }
    set Capacity(value){
        this._capacity = value;
    }

    //"default" getter and setter for the "_license" property
    get License(){
        return this._license;
    }
    set License(value){
        this._license = value;
    }

    //"default" getter and setter for the "_costPerDay" property
    get CostPerDay(){
        return this._costPerDay;
    }
    set CostPerDay(value){
        this._costPerDay = value;
    }

    //"default" getter and setter for the "_insurance" property
    get Insurance(){
        return this._insurance;
    }
    set Insurance(value){
        this._insurance = value;
    }

    //constructor for the "Van" Class
    constructor(vanID, capacity, license, costPerDay, insurance){
        this.VanID = vanID
        this.Capacity = capacity;
        this.License = license;
        this.CostPerDay = costPerDay;
        this.Insurance = insurance;
    }

    //returns a value of "_costPerDay" plus "_insurance"
    totalCost() {
        return Number(this._costPerDay) + Number(this._insurance);
    }

}


//| Main Program |

//creating an Instance a new object "van1" using "Van" class as a template
let van1 = new Van();

//asking user for "van1" property value:
van1.VanID = prompt("EZ Van Hire: Enter Van Details\n\nVan ID");
van1.Capacity = prompt("EZ Van Hire: Enter Van Details\n\nLoad Capacity (m3)");
van1.License = prompt("EZ Van Hire: Enter Van Details\n\nLicense Type");
van1.CostPerDay = Number(prompt("EZ Van Hire: Enter Van Details\n\nHire Cost Per Day"));
van1.Insurance = Number(prompt("EZ Van Hire: Enter Van Details\n\nInsurance Per Day"));

//using getters to display required output
console.log("EZ Van Hire: Van Details");
console.log("------------------------");
console.log(`Van ID\t\t\t\t\t:${van1.VanID}`);
console.log(`Load Capacity (m3): \t:${van1.Capacity}`);
console.log(`License Type \t\t\t:${van1.License}`);
console.log(`Hire Cost Per Day\t\t:$${van1.CostPerDay.toFixed(2)}`);
console.log(`Insurance Per Day\t\t:$${van1.Insurance.toFixed(2)}`);
console.log(`Total Hire Cost\t\t\t:$${van1.totalCost().toFixed(2)}`);
